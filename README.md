# README #

### What is this repository for? ###

* Use: to evaluate star rating of a sudoku based on its brute-force solve time 
* Version: 1.0

### How do I get set up? ###

*Requirement: Python 2.7*

1. git clone git@bitbucket.org:monkwoo/sudoku.git
2. cd /path/to/clone/directory
3. chmod +x ./sud
4. ./sud examples/*.test